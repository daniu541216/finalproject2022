# Introduction of the python project
## Resturant simulation
This is a Data analysis python project that aming to create a simulation of a resaurant with different types of customers.
the project consist of 4 parts. we coded part 1&2 together, part3&4 together.

### Part I Exploratory Data Analysis 
we do some basic data anlysis, liking plot the distribution of the clients, add additional columns for extra information.
1-make a plot of the distribution of the cost in each course
2-make a barplot of the cost per course.
3-add extra 6 additional columns for drinks and actual dishes. 

### Part II Clustering data
we are required to cluter the data by using kemeans and label the clients according to the result, then compare the result with the real sheet. 

### Part III Determing distributions
In this part, we compared our data analysis result to the real one, the we choose to use the real sheet part3.csv provided. 
we answered the questions:
1-the client type distribution
2-the likelihood of the certain client to get a certain course.
3-the probability of a certain type of customer ordering a certain dish.
4-the distribution of dishes, per course,per customer type.
5-distribution of the cost of the drink per course.

### Part IV, Simulation
based on the previous result and given conditions, using machine learning to create the simulation data.

