import pandas as pd
import matplotlib.pyplot as plt
from pandas import Series, DataFrame
from pandas.core.generic import NDFrame
from pandas.io.parsers import TextFileReader
from seaborn import barplot, histplot
### input the data
data1 = pd.read_csv('part1.csv')
# print(data1)

### make a plot of the distribution of the cost in each course
histplot(data=data1)
plt.show()

# sns.displot(
#     data=data1["FIRST_COURSE SECOND_COURSE THIRD_COURSE".split()]
# )
# plt.show()

### make a barplot of the cost per course
barplot(
    data=data1['FIRST_COURSE SECOND_COURSE THIRD_COURSE'.split()]
)
plt.show()


### add drinks and dishes to the column
##Starters:  Soup $3, Tomato-Mozarella $15, Oysters $20
##Mains: Salad $9, Spaghetti $20, Steak $25, Lobster $40
##Desserts: Ice cream $15, Pie $10
##price = nearest price  + drinks_price
##drinks_price = price - nearest_price
# firstly filter data
# list_1 =[3,15,20,9,20,25,40,15,10]
# list_1 = fillter(12)
#my_max(list_1)

def my_max(data_1ist):
    my_max = data_1ist[0]
    for i in data_1ist:
        if my_max < i:
           my_max = i
    return my_max
def fillter(input_data):    #data 是一个数字，例如12
    list_1 =[3,15,20,9,20,25,40,15,10]
    list_2 =[]
    for i in  list_1:
        if i < input_data :
            #print(i)
            list_2.append(i)
    return list_2

def find_drink_price(price):
    list_1 = fillter(price)
    if len(list_1) == 0:
        return 0
    dish_price = my_max(list_1)
    drink_price=  price - dish_price
    return drink_price

def find__dish_price(price):
    list_1 = fillter(price)
    if len(list_1) == 0:
        return 0
    dish_price = my_max(list_1)
    #drink_price=  price - dish_price
    return dish_price

### install
data1["drink_first"]= data1["FIRST_COURSE"].map(find_drink_price)
data1["drink_second"]= data1["SECOND_COURSE"].map(find_drink_price)
data1["drink_third"]= data1["THIRD_COURSE"].map(find_drink_price)

data1["dish_first"]= data1["FIRST_COURSE"].map(find__dish_price)
data1["dish_second"]= data1["SECOND_COURSE"].map(find__dish_price)
data1["dish_third"]= data1["THIRD_COURSE"].map(find__dish_price)

# print(data1)
# data1.to_csv('part1_1.csv',index=None)

###clustering the data by kmeans.
from sklearn.cluster import KMeans

##input the data and fit
mydata = KMeans(n_clusters=4, random_state=0).fit(data1[["FIRST_COURSE","SECOND_COURSE","THIRD_COURSE"]])
data1["label"] = mydata.predict(data1[["FIRST_COURSE","SECOND_COURSE","THIRD_COURSE"]])

# print(data1)

### after checking each label we can guess the potential clients type by the characters of the courses
# y= data1.groupby("label").mean()
# print(y)
print(data1.groupby("label").mean())

#0 businness      most expensive consumers
#1 healthy        the third course is lowerest
#2 retirement     three meals but less expensive compared with business
#3 one time       the second course are highest with the least first course and third course.

X =  data1.groupby("label").count()
X["CLIENT_ID"].plot.bar()
plt.show()

####transfer the label into client type
def label_trans(label):
    dict ={0:"businness",1:"healthy",2:"retirement",3:"one-time"}
    return dict[label]
data1["label"] = data1["label"].map(label_trans)

print(data1)

# data1.to_csv('part1&2.csv',index=None)