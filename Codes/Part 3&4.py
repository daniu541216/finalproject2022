import pandas as pd
import matplotlib.pyplot as plt
from bokeh.io import save, reset_output
from bokeh.layouts import gridplot
from bokeh.models import ColumnDataSource
from bokeh.palettes import Colorblind, Spectral
from bokeh.transform import cumsum, dodge
from numpy import pi
from bokeh.plotting import figure, output_file, show

###what type of the client

true_client_labels = pd.read_csv('part3.csv')
print(true_client_labels)
data3 = pd.read_csv('part1&2.csv')
try:
    if data3['label'] is not None:
        data3['label'] = true_client_labels['CLIENT_TYPE']
        data3.rename(columns={'label': 'CLIENT_TYPE'}, inplace=True)
except:
    pass


data3_grouped = true_client_labels.groupby("CLIENT_TYPE").count()
print(data3_grouped)
data3_grouped['prob'] = (data3_grouped['CLIENT_ID'] /
                         data3_grouped['CLIENT_ID'].sum())*100
data3_grouped['angle'] = data3_grouped['CLIENT_ID'] / \
    data3_grouped['CLIENT_ID'].sum() * 2*pi
data3_grouped['color'] = Colorblind[4]

p = figure(width=500, height=400, title="Distribution of Client Types", toolbar_location=None,
           tools="hover", tooltips="@CLIENT_TYPE: @CLIENT_ID (@prob%)", x_range=(-0.5, 1.0))

p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend_field='CLIENT_TYPE', source=data3_grouped)

p.axis.axis_label = None
p.axis.visible = False
p.grid.grid_line_color = None

# output_notebook()
output_file('Distribution of Client Types.html')
save(p)
reset_output()
show(p)

# reference: https://docs.bokeh.org/en/latest/docs/gallery/pie_chart.html
## reference:  https://docs.bokeh.org/en/latest/docs/user_guide/plotting.html

"""### Likelihood of Clients Getting Different Courses"""


def likelihood_of_client_getting_courses(client_type):

    # likelihood of certain type of client getting different courses
    type_grouped = true_client_labels.groupby("CLIENT_TYPE")
    type_total = type_grouped.get_group(client_type)['CLIENT_ID'].count()

    # likelihood of certain type of client getting starter
    type_starter_false = data3[(data3['FIRST_COURSE'] == 0) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    type_starter_likelihood = 1 - (type_starter_false / type_total)

    # likelihood of type client getting main
    type_main_false = data3[(data3['SECOND_COURSE'] == 0) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    type_main_likelihood = 1 - (type_main_false / type_total)

    # likelihood of type client getting dessert
    type_dessert_false = data3[(data3['THIRD_COURSE'] == 0) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    type_dessert_likelihood = 1 - (type_dessert_false / type_total)

    return [client_type, type_starter_likelihood, type_main_likelihood, type_dessert_likelihood]


likelihoods_business_client = likelihood_of_client_getting_courses('Business')
likelihoods_healthy_client = likelihood_of_client_getting_courses('Healthy')
likelihoods_onetime_client = likelihood_of_client_getting_courses('Onetime')
likelihoods_retirement_client = likelihood_of_client_getting_courses(
    'Retirement')

likelihoods_total = [likelihoods_business_client, likelihoods_healthy_client,
                     likelihoods_onetime_client, likelihoods_retirement_client]
likelihoods_distribution = pd.DataFrame(likelihoods_total, columns=[
                                        'CLIENT_TYPE', 'STARTER_LIKELIHOOD', 'MAIN_LIKELIHOOD', 'DESSERT_LIKELIHOOD'], dtype=float)
print(likelihoods_distribution)


client_types = ['Business', 'Healthy', 'Onetime', 'Retirement']
likelihoods_data = {'Client types': client_types,
                    'Starter': likelihoods_distribution['STARTER_LIKELIHOOD'].values.tolist(),
                    'Main': likelihoods_distribution['MAIN_LIKELIHOOD'].values.tolist(),
                    'Dessert': likelihoods_distribution['DESSERT_LIKELIHOOD'].values.tolist()
                    }

source = ColumnDataSource(data=likelihoods_data)

p = figure(x_range=client_types, y_range=(0, 1.5), title="Likelihoods Distribution of Client Types",
           height=350, toolbar_location=None)

p.vbar(x=dodge('Client types', -0.25, range=p.x_range), top='Starter', source=source,
       width=0.2, color="#c9d9d3", legend_label="Starter")

p.vbar(x=dodge('Client types',  0.0,  range=p.x_range), top='Main', source=source,
       width=0.2, color="#718dbf", legend_label="Main")

p.vbar(x=dodge('Client types',  0.25, range=p.x_range), top='Dessert', source=source,
       width=0.2, color="#e84d60", legend_label="Dessert")

p.x_range.range_padding = 0.1
p.xgrid.grid_line_color = None
p.legend.location = "top_left"
p.legend.orientation = "horizontal"

# output_notebook()
show(p)
output_file('Likelihood Distribution of Client Types.html')
save(p)
reset_output()

# reference: https://docs.bokeh.org/en/latest/docs/gallery/bar_dodged.html

"""### Likelihood of Clients Getting Different Courses"""


def likelihood_of_client_getting_courses(client_type):

    # likelihood of certain type of client getting different courses
    type_grouped = true_client_labels.groupby("CLIENT_TYPE")
    type_total = type_grouped.get_group(client_type)['CLIENT_ID'].count()

    # likelihood of certain type of client getting starter
    type_starter_false = data3[(data3['FIRST_COURSE'] == 0) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    type_starter_likelihood = 1 - (type_starter_false / type_total)

    # likelihood of type client getting main
    type_main_false = data3[(data3['SECOND_COURSE'] == 0) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    type_main_likelihood = 1 - (type_main_false / type_total)

    # likelihood of type client getting dessert
    type_dessert_false = data3[(data3['THIRD_COURSE'] == 0) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    type_dessert_likelihood = 1 - (type_dessert_false / type_total)

    return [client_type, type_starter_likelihood, type_main_likelihood, type_dessert_likelihood]


likelihoods_business_client = likelihood_of_client_getting_courses('Business')
likelihoods_healthy_client = likelihood_of_client_getting_courses('Healthy')
likelihoods_onetime_client = likelihood_of_client_getting_courses('Onetime')
likelihoods_retirement_client = likelihood_of_client_getting_courses(
    'Retirement')

likelihoods_total = [likelihoods_business_client, likelihoods_healthy_client,
                     likelihoods_onetime_client, likelihoods_retirement_client]
likelihoods_distribution = pd.DataFrame(likelihoods_total, columns=[
                                        'CLIENT_TYPE', 'STARTER_LIKELIHOOD', 'MAIN_LIKELIHOOD', 'DESSERT_LIKELIHOOD'], dtype=float)
print(likelihoods_distribution)
likelihoods_distribution.to_csv('part3_1.csv',index=None)


client_types = ['Business', 'Healthy', 'Onetime', 'Retirement']
likelihoods_data = {'Client types': client_types,
                    'Starter': likelihoods_distribution['STARTER_LIKELIHOOD'].values.tolist(),
                    'Main': likelihoods_distribution['MAIN_LIKELIHOOD'].values.tolist(),
                    'Dessert': likelihoods_distribution['DESSERT_LIKELIHOOD'].values.tolist()
                    }

source = ColumnDataSource(data=likelihoods_data)

p = figure(x_range=client_types, y_range=(0, 1.5), title="Likelihoods Distribution of Client Types",
           height=350, toolbar_location=None)

p.vbar(x=dodge('Client types', -0.25, range=p.x_range), top='Starter', source=source,
       width=0.2, color="#c9d9d3", legend_label="Starter")

p.vbar(x=dodge('Client types',  0.0,  range=p.x_range), top='Main', source=source,
       width=0.2, color="#718dbf", legend_label="Main")

p.vbar(x=dodge('Client types',  0.25, range=p.x_range), top='Dessert', source=source,
       width=0.2, color="#e84d60", legend_label="Dessert")

p.x_range.range_padding = 0.1
p.xgrid.grid_line_color = None
p.legend.location = "top_left"
p.legend.orientation = "horizontal"

# output_notebook()
show(p)
output_file('Likelihood Distribution of Client Types.html')
save(p)
reset_output()

# reference: https://docs.bokeh.org/en/latest/docs/gallery/bar_dodged.html

"""### Probability of a Certain Type of Customer Ordering a Certain Dish"""

# Starters:  Soup $3, Tomato-Mozarella $15, Oysters $20
# Mains: Salad $9, Spaghetti $20, Steak $25, Lobster $40
# Desserts: Ice cream $15, Pie $10
menu = {'dish_first':
        {
            'Soup': 3,
            'Tomato-Mozarella': 15,
            'Oysters': 20
        },
        'dish_second':
            {
                'Salad': 9,
                'Spaghetti': 20,
                'Steak': 25,
                'Lobster': 40
        },
        'dish_third':
            {
                'Ice cream': 15,
                'Pie': 10
        }
        }


def probability_certain_type_on_certain_dish(client_type, target_dish):
    target_course = ''
    dish_value = 0
    for course in menu:
        for dish in menu[course]:
            if dish == target_dish:
                target_course = course
                target_dish_value = menu[course][dish]
                break

    # print(target_course, target_dish, target_dish_value)
    target_client_type_course_grouped = data3[data3[target_course] > 0].groupby(
        "CLIENT_TYPE")
    target_client_type_course_total = target_client_type_course_grouped.get_group(
        client_type)['CLIENT_ID'].count()
    test = target_client_type_course_grouped.get_group(client_type)[
        'CLIENT_ID']
    # print(test)
    # likelihood of this type of client getting this dish
    target_dish_true = data3[(data3[target_course] == target_dish_value) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    # print('picked', target_dish_true)
    # print('total_hit', target_client_type_course_total)
    target_dish_likelihood = target_dish_true / target_client_type_course_total

    return target_course, target_dish_true, target_client_type_course_total, target_dish_likelihood


dish_types = ['Soup', 'Tomato-Mozarella', 'Oysters', 'Salad',
              'Spaghetti', 'Steak', 'Lobster', 'Ice cream', 'Pie']
dish_probability = []
dish_distribution = []
for client_type in client_types:
    temp1 = [client_type]
    for dish_type in dish_types:
        course, pick, total, prob = probability_certain_type_on_certain_dish(
            client_type, dish_type)
        temp1.append(prob.round(5))

    dish_probability.append(temp1)


dish_probability_per_type = pd.DataFrame(
    dish_probability, columns=['CLIENT-TYPE'] + dish_types)

print(dish_probability_per_type)
dish_probability_per_type.to_csv('part3_2.csv',index=None)

"""### Distribution of Dishes, per Course, per Customer Type"""


course_types = ['Starter', 'Main', 'Dessert']
client_type = 'Business'
distribution_per_course_per_type = []
for client_type in client_types:
    for course in menu:
        temp = [client_type, course]
        for dish in menu[course]:
            course, pick, total, prob = probability_certain_type_on_certain_dish(
                client_type, dish)
            temp.append(pick)
        distribution_per_course_per_type.append(temp)
# print(distribution_per_course_per_type)

total_distributions = []
for i in range(12):
    if i % 3 == 0:
        distribution = pd.DataFrame([distribution_per_course_per_type[i]], columns=[
                                    'CLIENT-TYPE', 'Course', 'Soup', 'Tomato-Mozarella', 'Oysters'])
    if i % 3 == 1:
        distribution = pd.DataFrame([distribution_per_course_per_type[i]], columns=[
                                    'CLIENT-TYPE', 'Course', 'Salad', 'Spaghetti', 'Steak', 'Lobster'])
    if i % 3 == 2:
        distribution = pd.DataFrame([distribution_per_course_per_type[i]], columns=[
                                    'CLIENT-TYPE', 'Course', 'Ice cream', 'Pie'])
    total_distributions.append(distribution)


def generate_distribution_graph(distribution):
    # print(distribution)
    client_type = distribution['CLIENT-TYPE'][0]
    course = distribution['Course'][0]
    if course == 'dish_first':
        course = 'Starter'
    elif course == 'dish_second':
        course = 'Main'
    else:
        course = 'Dessert'

    del distribution['CLIENT-TYPE']
    del distribution['Course']
    distribution_dict = distribution.to_dict('list')
    # print(distribution_dict)
    data = pd.Series(distribution_dict).reset_index(
        name='value').rename(columns={'index': 'dish'})
    for i in range(len(data)):
        data['value'][i] = pd.to_numeric(data['value'][i][0])

    # print(data)
    data['prob'] = (data['value'] / data['value'].sum())*100
    data['angle'] = data['value'] / data['value'].sum() * 2*pi
    # print('length:', len(data))
    if len(data) < 3:
        data['color'] = ['#35B778', '#FDE724']
    else:
        data['color'] = Colorblind[len(data)]

    p = figure(width=400, height=250, title="Distribution of Dishes among " + client_type + 'Clients Taking ' + course, toolbar_location=None,
               tools="hover", tooltips="@dish: @value (@prob%)", x_range=(-0.5, 1.0))

    p.wedge(x=0, y=1, radius=0.35,
            start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
            line_color="white", fill_color='color', legend_field='dish', source=data)

    p.axis.axis_label = None
    p.axis.visible = False
    p.grid.grid_line_color = None

    return p


business_graphs = []
healthy_graphs = []
onetime_graphs = []
retirement_graphs = []

for distribution in total_distributions:
    if distribution['CLIENT-TYPE'][0] == 'Business':
        graph = generate_distribution_graph(distribution)
        business_graphs.append(graph)
    elif distribution['CLIENT-TYPE'][0] == 'Healthy':
        graph = generate_distribution_graph(distribution)
        healthy_graphs.append(graph)
    elif distribution['CLIENT-TYPE'][0] == 'Onetime':
        graph = generate_distribution_graph(distribution)
        onetime_graphs.append(graph)
    else:
        graph = generate_distribution_graph(distribution)
        retirement_graphs.append(graph)

grid = gridplot([business_graphs, healthy_graphs,
                onetime_graphs, retirement_graphs])

# output_notebook()
show(grid)
output_file('Distribution of Dishes.html')
save(grid)
reset_output()
# output_graphs.append(grid)

# reference: https://docs.bokeh.org/en/latest/docs/gallery/pie_chart.html
"""### Distribution of the Cost of the Drinks per Course"""


def generate_drink_cost_distribution(course):

    course_type = ''
    if course == 'Starter':
        course_type = 'drink_first'
    elif course == 'Main':
        course_type = 'drink_second'
    else:
        course_type = 'drink_third'

    value_cut = data3[course_type].value_counts(bins=10, sort=False)

    # converting to df and assigning new names to the columns
    data3_cut = pd.DataFrame(value_cut)
    data3_cut = data3_cut.reset_index()
    data3_cut.columns = ['Range', 'Count']  # change column names
    data3_cut['Range'] = data3_cut['Range'].astype(str)
    data3_cut['color'] = Spectral[10]
    data3_cut['prob'] = (data3_cut['Count'] / data3_cut['Count'].sum())*100
    source = ColumnDataSource(data=data3_cut)

    p = figure(x_range=data3_cut['Range'], y_range=(0, data3_cut['Count'].max()*1.2), width=1000, height=500, title='Distribution of the Cost of the Drinks in ' + course,
               toolbar_location=None, tools="hover", tooltips="$@Range: @Count (@prob%)")

    p.vbar(x='Range', top='Count', width=0.8, color='color',
           legend_field="Range", source=source)

    p.xgrid.grid_line_color = None
    p.legend.orientation = "horizontal"
    p.legend.location = "top_center"

    # output_notebook()

    show(p)
    output_file('Distribution of Drinks - ' + course + '.html')
    save(p)
    reset_output()


course_types = ['Starter', 'Main', 'Dessert']

for course in course_types:
    generate_drink_cost_distribution(course)

# reference:
#   https://re-thought.com/pandas-value_counts/
#   https://docs.bokeh.org/en/latest/docs/gallery/bar_colors.html

"""## Part 4"""


def likelihood_of_client_entering_restaurant(client_type):

    type_grouped = true_client_labels.groupby("CLIENT_TYPE")
    type_total = type_grouped.get_group(client_type)['CLIENT_ID'].count()

    type_starter_false = data3[(data3['FIRST_COURSE'] == 0) & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    type_starter_likelihood = 1 - (type_starter_false / type_total)

    getting_lunch = data3[(data3['TIME'] == 'LUNCH') & (
        data3['CLIENT_TYPE'] == client_type)]['CLIENT_ID'].count()
    getting_lunch_likelihood = getting_lunch / type_total
    getting_dinner_likelihood = 1 - getting_lunch_likelihood

    return [client_type, getting_lunch_likelihood, getting_dinner_likelihood]


likelihood_of_business_client_entering_restaurant = likelihood_of_client_entering_restaurant(
    'Business')
likelihood_of_healthy_client_entering_restaurant = likelihood_of_client_entering_restaurant(
    'Healthy')
likelihood_of_onetime_client_entering_restaurant = likelihood_of_client_entering_restaurant(
    'Onetime')
likelihood_of_retirement_client_entering_restaurant = likelihood_of_client_entering_restaurant(
    'Retirement')


likelihoods_time_total = [likelihood_of_business_client_entering_restaurant, likelihood_of_healthy_client_entering_restaurant,
                          likelihood_of_onetime_client_entering_restaurant, likelihood_of_retirement_client_entering_restaurant]
likelihoods_time_distribution = pd.DataFrame(likelihoods_time_total, columns=[
                                             'CLIENT_TYPE', 'LUNCH_LIKELIHOOD', 'DINNER_LIKELIHOOD'])


global_client_id = 3


class Customer:

    global global_client_id

    def __init__(self, client_type):
        self.info = []
        self.client_type = client_type
        self.time = ''
        self.client_id = ''
        self.client_return = ''
        self.starter_course = ''
        self.main_course = ''
        self.dessert_course = ''
        self.first_drink = ''
        self.second_drink = ''
        self.third_drink = ''
        self.course_count = 0

        global global_client_id

        def generate_time(c_type):
            choices = ['LUNCH', 'DINNER']
            result = random.choices(choices, weights=(likelihoods_time_distribution[(likelihoods_time_distribution['CLIENT_TYPE'] == c_type)]['LUNCH_LIKELIHOOD'].values[0], likelihoods_time_distribution[(
                likelihoods_time_distribution['CLIENT_TYPE'] == c_type)]['DINNER_LIKELIHOOD'].values[0]), k=1)
            return result[0]

        def generate_id(c_type):
            global global_client_id

            regular = 0
            if c_type == 'Business':
                regular = 0.5
            elif c_type == 'Healthy':
                regular = 0.3
            elif c_type == 'Retirement':
                regular = 0.1
            else:
                regular = 1

            choices = ['REGULAR', 'RETURN']
            client_return = random.choices(
                choices, weights=(regular, 1-regular), k=1)[0]

            if client_return == 'RETURN':
                if c_type == 'Business':
                    return_id = 0
                elif c_type == 'Healthy':
                    return_id = 1
                elif c_type == 'Retirement':
                    return_id = 2
                else:
                    return_id = 3

                full_id = str(return_id).zfill(7)
                client_id = 'ID' + full_id
            else:
                global_client_id += 1
                full_id = str(global_client_id).zfill(7)
                client_id = 'ID' + full_id

            return client_id, client_return

        def order_drink(course):
            course_type = ''

            if course == 'Starter':
                course_type = 'drink_first'
            elif course == 'Main':
                course_type = 'drink_second'
            else:
                course_type = 'drink_third'

            value_cut = data3[course_type].value_counts(bins=10, sort=False)

            # converting to df and assigning new names to the columns
            data3_cut = pd.DataFrame(value_cut)
            data3_cut = data3_cut.reset_index()
            data3_cut.columns = ['Range', 'Count']  # change column names
            data3_cut['Range'] = data3_cut['Range'].astype(str)

            choices = data3_cut['Range'].values
            weights = data3_cut['Count'].values
            drink_taking = random.choices(choices, weights=weights, k=1)[0]

            return drink_taking

        def order_course(self, c_type, course_type):
            if course_type == 'Starter':
                course_likelihood = 'STARTER_LIKELIHOOD'
            elif course_type == 'Main':
                course_likelihood = 'MAIN_LIKELIHOOD'
            else:
                course_likelihood = 'DESSERT_LIKELIHOOD'

            choices = ['YES', 'NO']
            likelihood = likelihoods_distribution[(
                likelihoods_distribution['CLIENT_TYPE'] == c_type)][course_likelihood].values[0]

            taking_course = random.choices(
                choices, weights=(likelihood, 1-likelihood), k=1)[0]

            weights = None

            if taking_course == 'YES':
                self.course_count += 1
                if c_type == 'Business':
                    if course_type == 'Starter':
                        choices = list(total_distributions[0].columns)
                        weights = tuple(
                            total_distributions[0].values.tolist()[0])
                        drink_taking = order_drink('Starter')
                    if course_type == 'Main':
                        choices = list(total_distributions[1].columns)
                        weights = tuple(
                            total_distributions[1].values.tolist()[0])
                        drink_taking = order_drink('Main')
                    if course_type == 'Dessert':
                        choices = list(total_distributions[2].columns)
                        weights = tuple(
                            total_distributions[2].values.tolist()[0])
                        drink_taking = order_drink('Dessert')

                if c_type == 'Healthy':
                    if course_type == 'Starter':
                        choices = list(total_distributions[3].columns)
                        weights = tuple(
                            total_distributions[3].values.tolist()[0])
                        drink_taking = order_drink('Starter')
                    if course_type == 'Main':
                        choices = list(total_distributions[4].columns)
                        weights = tuple(
                            total_distributions[4].values.tolist()[0])
                        drink_taking = order_drink('Main')
                    if course_type == 'Dessert':
                        choices = list(total_distributions[5].columns)
                        weights = tuple(
                            total_distributions[5].values.tolist()[0])
                        drink_taking = order_drink('Dessert')

                if c_type == 'Onetime':
                    if course_type == 'Starter':
                        choices = list(total_distributions[6].columns)
                        weights = tuple(
                            total_distributions[6].values.tolist()[0])
                        drink_taking = order_drink('Starter')
                    if course_type == 'Main':
                        choices = list(total_distributions[7].columns)
                        weights = tuple(
                            total_distributions[7].values.tolist()[0])
                        drink_taking = order_drink('Main')
                    if course_type == 'Dessert':
                        choices = list(total_distributions[8].columns)
                        weights = tuple(
                            total_distributions[8].values.tolist()[0])
                        drink_taking = order_drink('Dessert')

                if c_type == 'Retirement':
                    if course_type == 'Starter':
                        choices = list(total_distributions[9].columns)
                        weights = tuple(
                            total_distributions[9].values.tolist()[0])
                        drink_taking = order_drink('Starter')
                    if course_type == 'Main':
                        choices = list(total_distributions[10].columns)
                        weights = tuple(
                            total_distributions[10].values.tolist()[0])
                        drink_taking = order_drink('Main')
                    if course_type == 'Dessert':
                        choices = list(total_distributions[11].columns)
                        weights = tuple(
                            total_distributions[11].values.tolist()[0])
                        drink_taking = order_drink('Dessert')

                dish_taking = random.choices(choices, weights=weights, k=1)[0]

                drink_taking = drink_taking[1:-1].split(', ')
                drink_taking_price = random.uniform(
                    float(drink_taking[0]), float(drink_taking[1]))
                return dish_taking, round(drink_taking_price, 2)

            else:
                return 'nothing', 'nothing'

        def go_to_restaurant(self, c_type):
            starter_course, first_drink = order_course(self, c_type, 'Starter')
            main_course, second_drink = order_course(self, c_type, 'Main')
            dessert_course, third_drink = order_course(self, c_type, 'Dessert')

            return starter_course, main_course, dessert_course, first_drink, second_drink, third_drink

        def get_dish_price(menu, dish_name):
            for course in menu:
                for dish in menu[course]:
                    if dish == dish_name:
                        return menu[course][dish]
            return 0

        def generate_data(self):

            starter_price = get_dish_price(menu, self.starter_course)
            main_price = get_dish_price(menu, self.main_course)
            dessert_price = get_dish_price(menu, self.dessert_course)

            if self.starter_course != 'nothing':
                starter_total = round(
                    starter_price + float(self.first_drink), 2)
            else:
                starter_total = 0

            if self.main_course != 'nothing':
                main_total = round(main_price + float(self.second_drink), 2)
            else:
                main_total = 0

            if self.dessert_course != 'nothing':
                dessert_total = round(
                    dessert_price + float(self.third_drink), 2)
            else:
                dessert_total = 0
            info = [self.time, self.client_id, self.client_type, self.starter_course, self.main_course, self.dessert_course,
                    self.first_drink, self.second_drink, self.third_drink, starter_total, main_total, dessert_total]
            return info

        self.time = generate_time(client_type)
        self.client_id, self.client_return = generate_id(client_type)
        self.starter_course, self.main_course, self.dessert_course, self.first_drink, self.second_drink, self.third_drink = go_to_restaurant(
            self, client_type)

        print(self.client_id + ' client is a ' + self.client_return.lower() + ' customer and taking ' +
              self.starter_course + ' as starter, ' + self.main_course + ' as main, and ' + self.dessert_course + ' as dessert.')

        self.info = generate_data(self)
        # print(self.info)


def create_customer():
    choices = client_types
    data3_grouped_reset = data3_grouped.reset_index()
    weights = (data3_grouped_reset[(data3_grouped_reset['CLIENT_TYPE'] == 'Business')]['CLIENT_ID'].values[0], data3_grouped_reset[(data3_grouped_reset['CLIENT_TYPE'] == 'Healthy')]['CLIENT_ID'].values[0],
               data3_grouped_reset[(data3_grouped_reset['CLIENT_TYPE'] == 'Onetime')]['CLIENT_ID'].values[0], data3_grouped_reset[(data3_grouped_reset['CLIENT_TYPE'] == 'Retirement')]['CLIENT_ID'].values[0])

    type_generating = random.choices(choices, weights=weights, k=1)[0]
    customer = Customer(type_generating)

    return customer


total_num_courses = 5*365*20
#total_num_courses = 100
course_count = 0
i = 0
data_generated_list = []

while course_count <= total_num_courses:
    c = create_customer()
    data_generated_list.append(c.info)
    course_count += c.course_count

data_generated = pd.DataFrame(data_generated_list, columns=('TIME', 'CUSTOMERID', 'CUSTOMERTYPE',
                              'COURSE1', 'COURSE2', 'COURSE3', 'DRINKS1', 'DRINKS2', 'DRINKS3', 'TOTAL1', 'TOTAL2', 'TOTAL3'))

print(data_generated)

data_generated.to_csv('part4_data_generated.csv', index=None)
